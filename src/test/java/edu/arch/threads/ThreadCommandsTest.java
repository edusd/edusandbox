package edu.arch.threads;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.concurrent.LinkedBlockingQueue;

class ThreadCommandsTest {

    @Test
    void startTest() {
        //Given
        var q = new LinkedBlockingQueue<Command>();
        var server = new ThreadCommands(q);

        //When
        server.start();

        //Then
        Assertions.assertTrue(server.isAlive());
        server.stop();
    }

    @Test
    void hardStopTest() {
        //Given
        var q = new LinkedBlockingQueue<Command>();
        var server = new ThreadCommands(q);

        var msgCommand = Mockito.mock(MessageCommand.class);

        q.offer(msgCommand);
        q.offer(msgCommand);
        q.offer(new HardStopCommand(server));
        q.offer(msgCommand);

        //When
        server.start();
        while (server.isAlive()) {}

        //Then
        Mockito.verify(msgCommand, Mockito.times(2)).execute();
    }

    @Test
    void softStopTest() {
        //Given
        var q = new LinkedBlockingQueue<Command>();
        var server = new ThreadCommands(q);

        var msgCommand = Mockito.mock(MessageCommand.class);

        q.offer(msgCommand);
        q.offer(msgCommand);
        q.offer(new SoftStopCommand(server, q));
        q.offer(msgCommand);

        //When
        server.start();
        while (server.isAlive()) {}

        //Then
        Mockito.verify(msgCommand, Mockito.times(3)).execute();
    }

}