package edu.arch.patterns.adapter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MatrixServiceTest {

    @Test
    void genMatrix() {
        int h = 4;
        int w = 3;
        var matrix = MatrixService.genMatrix(w, h);
        Assertions.assertEquals(h, matrix.length);
        Assertions.assertEquals(w, matrix[0].length);
    }

    @Test
    void addMatrix() {
        int h = 4;
        int w = 3;
        var matrix1 = MatrixService.genMatrix(w, h);
        var matrix2 = MatrixService.genMatrix(w, h);
        var result = MatrixService.addMatrix(w, h, matrix1, matrix2);
        Assertions.assertEquals(h, result.length);
        Assertions.assertEquals(w, result[0].length);
        Assertions.assertEquals(matrix1[0][0] + matrix2[0][0], result[0][0]);
    }

}
