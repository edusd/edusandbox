package edu.arch.patterns.adapter;

import java.util.Random;

public class MatrixService {
    private static final Random randomGenerator = new Random();

    public static int[][] genMatrix(int w, int h) {
        var matrix = new int[h][w];
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                matrix[i][j] = randomGenerator.nextInt(0, 10);
            }
        }
        return matrix;
    }

    public static int[][] addMatrix(int w, int h, int[][] matrix1, int[][] matrix2) {
        return new MatrixAdderAdapter().addMatrix(w, h, matrix1, matrix2);
    }

    public static void main(String[] args) {
        int h = 4;
        int w = 3;
        var matrix1 = genMatrix(w, h);
        var matrix2 = genMatrix(w, h);
        var result = addMatrix(w, h, matrix1, matrix2);
    }
}
