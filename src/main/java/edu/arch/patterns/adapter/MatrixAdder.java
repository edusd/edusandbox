package edu.arch.patterns.adapter;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class MatrixAdder {
    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            throw new RuntimeException("Arguments not enough!");
        }
        Path inFilePath = Paths.get(args[0]);
        Path outFilePath = Paths.get(args[1]);

        int w;
        int h;
        int[][] matrix1;
        int[][] matrix2;
        try (Reader reader =  new FileReader(inFilePath.toFile())) {
            Scanner scanner = new Scanner(reader).useDelimiter("\n");
            String[] sizes = scanner.next().split(" ");
            w = Integer.parseInt(sizes[0]);
            h = Integer.parseInt(sizes[1]);

            matrix1 = new int[h][w];
            matrix2 = new int[h][w];

            for (int i = 0; i < h; i++) {
                String[] line = scanner.next().split(" ");
                for (int j = 0; j < w; j++) {
                    matrix1[i][j] = Integer.parseInt(line[j]);
                }
            }
            scanner.next();
            for (int i = 0; i < h; i++) {
                String[] line = scanner.next().split(" ");
                for (int j = 0; j < w; j++) {
                    matrix2[i][j] = Integer.parseInt(line[j]);
                }
            }
        }

        try (Writer writer = new FileWriter(outFilePath.toFile())) {
            for (int i = 0; i < h; i++) {
                for (int j = 0; j < w; j++) {
                    int sum = matrix1[i][j] + matrix2[i][j];
                    writer.write(String.valueOf(sum));
                    if (j != w - 1) {
                        writer.write(' ');
                    }
                }
                if (i != h - 1) {
                    writer.write('\n');
                }
            }
        }
    }
}
