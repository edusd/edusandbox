package edu.arch.patterns.adapter;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Scanner;

public class MatrixAdderAdapter {

    private String matrixToStr(int w, int h, int[][] matrix) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                sb.append(matrix[i][j]);
                if (j != w - 1) {
                    sb.append(' ');
                }
            }
            if (i != h - 1) {
                sb.append('\n');
            }
        }
        return sb.toString();
    }

    public int[][] addMatrix(int w, int h, int[][] matrix1, int[][] matrix2) {
        String inFilePath = "./build/tmp/matrix.txt";
        String outFilePath = "./build/tmp/matrix_add.txt";

        try (Writer writer = new FileWriter(inFilePath)) {
            writer.write("%d %d%n".formatted(w, h));
            writer.write(matrixToStr(w, h, matrix1));
            writer.write("\n\n");
            writer.write(matrixToStr(w, h, matrix2));
        } catch (IOException e) {
            throw new RuntimeException("Error write matrix: ", e);
        }

        try {
            MatrixAdder.main(new String[] {inFilePath, outFilePath});
        } catch (IOException e) {
            throw new RuntimeException("Error call adder program: ", e);
        }

        int[][] result = new int[h][w];
        try (Reader reader =  new FileReader(outFilePath)) {
            Scanner scanner = new Scanner(reader).useDelimiter("\n");

            for (int i = 0; i < h; i++) {
                String[] line = scanner.next().split(" ");
                for (int j = 0; j < w; j++) {
                    result[i][j] = Integer.parseInt(line[j]);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Error read result", e);
        }

        return result;
    }
}
