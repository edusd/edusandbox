package edu.arch.threads;

public interface Command {
    void execute();
}
