package edu.arch.threads;

import java.util.concurrent.BlockingQueue;
import java.util.function.BooleanSupplier;

public class ThreadCommands {
    private final Thread thread;
    private boolean isStop;
    private BooleanSupplier stopCheck;

    public ThreadCommands(BlockingQueue<Command> q) {
        this.isStop = false;
        this.stopCheck = () -> !this.isStop;
        Runnable behavior = () -> {
            while (this.stopCheck.getAsBoolean()) {
                try {
                    var c = q.take();
                    c.execute();
                } catch (Exception e) {
                    // ExceptionHander
                }
            }
        };
        this.thread = new Thread(behavior);
    }

    public boolean isAlive() {
        return this.thread.isAlive();
    }

    public void start() {
        this.thread.start();
    }

    public void stop() {
        this.isStop = true;
    }

    public void setStopCheck(BooleanSupplier stopCheck) {
        this.stopCheck = stopCheck;
    }
}
