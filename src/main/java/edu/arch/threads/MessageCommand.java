package edu.arch.threads;

public class MessageCommand implements Command {
    private final String msg;
    public MessageCommand(String msg) {
        this.msg = msg;
    }

    @Override
    public void execute() {
        System.out.println(msg);
    }
}
