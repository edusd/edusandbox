package edu.arch.threads;

import java.util.concurrent.LinkedBlockingQueue;

public class Program {
    public static void main(String[] args) {
        var q = new LinkedBlockingQueue<Command>();
        var server = new ThreadCommands(q);

        q.offer(new MessageCommand("Command 1"));
        q.offer(new MessageCommand("Command 2"));
//        q.offer(new HardStopCommand(server));
        q.offer(new SoftStopCommand(server, q));
        q.offer(new MessageCommand("Command 3"));

        server.start();
    }
}
