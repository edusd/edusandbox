package edu.arch.threads;

public class HardStopCommand implements Command {
    private final ThreadCommands server;
    public HardStopCommand(ThreadCommands server) {
        this.server = server;
    }

    @Override
    public void execute() {
        System.out.println("Hard stop!");
        server.stop();
    }
}
