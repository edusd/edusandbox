package edu.arch.threads;

import java.util.concurrent.BlockingQueue;

public class SoftStopCommand implements Command {
    private final ThreadCommands server;
    private final BlockingQueue<Command> q;
    public SoftStopCommand(ThreadCommands server, BlockingQueue<Command> q) {
        this.server = server;
        this.q = q;
    }

    @Override
    public void execute() {
        System.out.println("Soft stop!");
        server.setStopCheck(() -> !q.isEmpty());
    }
}
